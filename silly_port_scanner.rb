#! /usr/bin/ruby
#
require 'socket'
require 'timeout'

port = ARGV[0]
while true do
  loop do
    ip = sprintf('%d.%d.%d.%d', srand % 256, srand % 256, srand % 256, srand % 256)
    printf("\e[32mChecking %s:%d...\e[0m", ip, port)

    connected = begin
      Timeout::timeout(10) { TCPSocket.new(ip, port).close }
    rescue
      false
    end
    connected ? printf("\e[32mSuccess!\e[0m\n") : printf("\e[31mFailure\e[0m\n")
  end
end
